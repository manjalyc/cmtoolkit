# cmtoolkit

Sets of commonly used python functionality and utilities. Akin to [cmutils](https://gitlab.com/manjalyc/cmutils) but with a commitment to a slow-rolling, continously tested, non-breaking, stable interface for long-term deployments. Uses cmutils behind the scenes.
